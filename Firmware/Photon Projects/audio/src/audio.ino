/*
 * Project audio
 * Description:
 * Author:
 * Date:
 */


 // This #include statement was automatically added by the Spark IDE.
 #include "Adafruit_VS1053_Photon.h"
 #include "sd-card-library-photon-compat.h"

 /***************************************************
   This is an example for the Adafruit VS1053 Codec Breakout
   Designed specifically to work with the Adafruit VS1053 Codec Breakout
   ----> https://www.adafruit.com/products/1381
   Adafruit invests time and resources providing this open source code,
   please support Adafruit and open-source hardware by purchasing
   products from Adafruit!
   Written by Limor Fried/Ladyada for Adafruit Industries.
   BSD license, all text above must be included in any redistribution
  ****************************************************/


#define BREAKOUT_SCK     A3
#define BREAKOUT_MISO    A4
#define BREAKOUT_MOSI    A5
 // These are the pins used for the breakout example
 #define BREAKOUT_RESET  D6//9      // VS1053 reset pin (output)
 #define BREAKOUT_CS     A2//10     // VS1053 chip select pin (output)
 #define BREAKOUT_DCS    D5//8      // VS1053 Data/command select pin (output)
 // These are the pins used for the music maker shield

 // These are common pins between breakout and shield
 #define CARDCS D3//4     // Card chip select pin
 // DREQ should be an Int pin, see http://arduino.cc/en/Reference/attachInterrupt
 #define DREQ WKP//3       // VS1053 Data request, ideally an Interrupt pin

//Adafruit_VS1053_FilePlayer (int8_t mosi, int8_t miso, int8_t clk,
  //        int8_t rst, int8_t cs, int8_t dcs, int8_t dreq,
//          int8_t cardCS);
 //for triggering to play the file:
 //#define TRIGGERPIN D1

 Adafruit_VS1053_FilePlayer musicPlayer =
   // create breakout-example object!
   Adafruit_VS1053_FilePlayer(BREAKOUT_MOSI, BREAKOUT_MISO, BREAKOUT_SCK, BREAKOUT_RESET, BREAKOUT_CS, BREAKOUT_DCS, DREQ, CARDCS);

 bool playing=false;


const int p_button = D1;     // the number of the pushbutton pin

uint8_t buttonState,
        prevButtonState;

void setup() {
  Serial.begin(9600);
  Serial.println("Adafruit VS1053 Simple Test");

  if (! musicPlayer.begin()) { // initialise the music player
     Serial.println(F("Couldn't find VS1053, do you have the right pins defined?"));
     while (1);
  }
  Serial.println(F("VS1053 found"));

  SD.begin(BREAKOUT_MOSI,BREAKOUT_MISO,BREAKOUT_SCK,CARDCS);    // initialise the SD card

  // Set volume for left, right channels. lower numbers == louder volume!
  musicPlayer.setVolume(20,20);

  // Timer interrupts are not suggested, better to use DREQ interrupt!
  //musicPlayer.useInterrupt(VS1053_FILEPLAYER_TIMER0_INT); // timer int

  // If DREQ is on an interrupt pin (on uno, #2 or #3) we can do background
  // audio playing
  musicPlayer.useInterrupt(VS1053_FILEPLAYER_PIN_INT);  // DREQ int

  pinMode(p_button, INPUT_PULLUP);
  buttonState = HIGH;
  prevButtonState = HIGH;

}


void loop()
{
 //////////////////////////////////////////////////////////////////////////////////////////////

  buttonState = debounceRead(p_button);
  if ((LOW == buttonState) && (buttonState != prevButtonState)) //if button was just pressed
  {

    if (musicPlayer.stopped())
    {
      if (! musicPlayer.startPlayingFile("RECORD00.ogg")) {
        Serial.println("Could not open file track002.mp3");
        while (1);
      }
      Serial.println(F("Started playing"));
    }
    else
    {
      musicPlayer.stopPlaying();
    }
  }
  prevButtonState = buttonState;  //update prevButtonState

/////////////////////////////////////////////////////////////////////////////////////////////
}

//Use like digitalRead. Incorporates button debouncing.
uint8_t debounceRead(int pin)
{
  uint8_t pinState = digitalRead(pin);
  uint32_t timeout = millis();
  while (millis() < timeout+10)
  {
    if (digitalRead(pin) != pinState)
    {
      pinState = digitalRead(pin);
      timeout = millis();
    }
  }

  return pinState;
}
